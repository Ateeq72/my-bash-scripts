## Add this file as bash_aliases in ~/.bash_rc
# You may need to add something like
# 
# MY_BASH_ALIASES="~/Documents/My_Scripts/bash_aliases"
# if [ -f ${MY_BASH_ALIASES} ]; then
#     . ${MY_BASH_ALIASES}
#         echo "Loaded : "${MY_BASH_ALIASES}
#         fi
#

#alias composer='/home/ateeq-ahmed/Documents/composer/./composer.phar'
alias composer-dump='composer dump-autoload --optimize'
alias fix-pdf='cp ../Downloads/wkhtmltox/bin/wkhtmltopdf vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'
alias unfix-pdf='git co -- vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'
alias nginx_restart='sudo systemctl restart nginx-stack.service'
alias youtube-dl-mp3='youtube-dl -x --audio-quality 9 --audio-format mp3'
alias git_stat="git fetch origin && git status &&  git branch -vv | grep -E 'ahead|behind'"
alias rm_orig='for files in `find ./ -name *.orig`; do rm $files; done'
alias rm_temp='sudo rm -rf /tmp/* /var/tmp/*'
alias fetch_behind='git fetch origin && git_pull && git branch -v | grep -E "behind" | sed -E "s/[ *] ([^[:space:]]+).*()/\1/g" | while read line; do echo pulling $line; git fetch origin $line:$line ; done'
alias push_ahead='git fetch origin && git branch -v | grep -E "ahead" | sed -E "s/[ *] ([^[:space:]]+).*()/\1/g" | while read line; do echo pushing $line; git push origin $line ; done'
alias azher_halframework_mount='sshfs azher@halframework.azher:/Users/azher/sites/halframework -o nonempty /home/ateeq-ahmed/azher_halframework'
alias azher_halframework_wip_mount='sshfs azher@halframework.azher:/Users/azher/sites/halframework_wip -o nonempty /home/ateeq-ahmed/azher_halframework_wip'
alias azher_home_mount='sshfs azher@halframework.azher:/Users/azher -o nonempty /home/ateeq-ahmed/azher_Mac_home'
alias azher_halframework_unmount='fusermount -u /home/ateeq-ahmed/azher_halframework'
alias azher_halframework_wip_unmount='fusermount -u /home/ateeq-ahmed/azher_halframework_wip'
alias azher_home_unmount='fusermount -u /home/ateeq-ahmed/azher_Mac_home'
alias azher_halframework_xdebug='ssh -R 9000:localhost:9000 azher@halframework.azher'
alias zrehan_home_mount='sshfs zrehan@zrehan.pc:/home/zrehan -o nonempty /home/ateeq-ahmed/zrehan_home'
alias zrehan_home_unmount='fusermount -u /home/ateeq-ahmed/zrehan_home'
alias sudo-docker-compose="sudo docker-compose"
alias use_docker="sudo /opt/nginxstack-1.10.1-1/ctlscript.sh stop && sudo systemctl stop redis && curDir=`pwd` && echo ${curDir} && cd /home/ateeq-ahmed/halframework/dockerdir && start_docker && cd ${curDir} && echo ${curDir}"
alias use_stack="curDir=`pwd` && echo ${cirDir} && cd /home/ateeq-ahmed/halframework/dockerdir && stop_docker && cd ${curDir} && sudo /opt/nginxstack-1.10.1-1/ctlscript.sh start && sudo systemctl start redis"
alias start_docker="sudo-docker-compose up -d"
alias stop_docker="sudo-docker-compose down"
alias stop_docker="sudo-docker-compose down"
alias git_reset="git add . && git reset --hard"
alias git_reset_remote='git add . && git reset --hard $(git rev-parse --abbrev-ref --symbolic-full-name @{u})'

##
# From Home PC
#



alias youtube-dl-mp3='youtube-dl -x --audio-format mp3 --audio-quality 0'
alias ateeq-pc='ssh -X ateeq@ateeq-pc'
alias ateeq-rpi='ssh -X pi@ateeq-rpi'
alias sshfs-ateeq-pc='sshfs ateeq@ateeq-pc:/home/ateeq/ /run/media/ateeq-ahmed/ATEEQ-PC'
alias sshfs-ateeq-rpi='if [[ ! -d "/run/media/ateeq-ahmed/ATEEQ-RPI" ]]; then sudo mkdir -p /run/media/ateeq-ahmed/ATEEQ-RPI && sudo chown -R ateeq-ahmed:ateeq-ahmed /run/media/ateeq-ahmed/ATEEQ-RPI; else rm -rf /run/media/ateeq-ahmed/ATEEQ-RPI/*; fi && sshfs pi@ateeq-rpi:/home/pi/ /run/media/ateeq-ahmed/ATEEQ-RPI'
alias unsshfs-ateeq-rpi='fusermount -u /run/media/ateeq-ahmed/ATEEQ-RPI && if [[ -d  "/media/ateeq-ahmed/ATEEQ-RPI/*" ]]; then rm -rf /media/ateeq-ahmed/ATEEQ-RPI/*; fi'
alias weka='java -jar /home/ateeq-ahmed/Downloads/weka-3-8-0/weka.jar'
alias hal='cd /Users/ateeq-ahmed/work/Development/halframework'
alias hal_vatcare='cd /home/ateeq-ahmed/Documents/work/hal_vatcare'
alias hobby='cd /home/ateeq-ahmed/Documents/hobby'
alias hal_reachapp='cd /Users/ateeq-ahmed/work/Development/hal_reachapp'
alias downloads_hdd='cd /run/media/ateeq-ahmed/EXT4_HDD/Downloads'
alias wdownloads_hdd='cd /run/media/ateeq-ahmed/EXT4_HDD/Downloads/Work_Downloads'
alias documents_hdd='cd /run/media/ateeq-ahmed/EXT4_HDD/Documents'
alias deploy_apk_oneplus='flutter build apk --target-platform  android-arm64 && adb -s 192.168.0.31 unroot && adb  -s 192.168.0.31 install build/app/outputs/apk/release/app-release.apk'
alias laptop_lid_state='watch -n 1 cat /proc/acpi/button/lid/LID0/state'

#alias hal='cd /home/ateeq-ahmed/halframework'

alias fix-pdf-home-lp='cp /home/ateeq-ahmed/Downloads/wkhtmltox/bin/wkhtmltopdf /home/ateeq-ahmed/Documents/work/halframework/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'
alias unfix-pdf-home-lp='git co -- vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'
alias import_schema=' ls *.sql | while read line; do echo $line; line=`basename $line .sql`; echo "Backing Up ${line}!"; mysqldump -uroot $line > ${line}_`date +%Y-%m-%d:%H:%M:%S`_bak.sql ; echo "Done Backing Up!"; echo "Dropping Tables in $line"; mysqldump -uroot $line --add-drop-table | grep "DROP TABLE" | mysql -uroot $line ; echo "Done Dropping"; echo "Importing $line"; mysql -uroot $line < $line.sql; echo "Done importing $line"; done'
alias vga_card_status='sudo cat /sys/kernel/debug/vgaswitcheroo/switch'
alias use_dgpu_amd='DRI_PRIME=1'
alias use_dgpu_nvidia='__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia'
alias phpd='php -dxdebug.remote_autostart'
alias steam="LIBGL_DRI3_DISABLE=1 /usr/bin/steam"
alias cp_using_rsync="rsync -a -v --info=progress2 --ignore-existing "
alias hal_deploy="echo -e '\nPushing to origin...\n'; push_ahead && echo -e '\nPushing SAAS-DEV to Linode...\n'; git push SAAS-DEV-LINODE SAAS-DEV && echo -e '\nPushing SAAS-LIVE to Linode...\n'; git push SAAS-LINODE SAAS-LIVE && echo -e '\nPushing SAAS-ISSAM to Linode...\n'; git push SAAS-ISSAM SAAS-ISSAM"
alias hal_gulp_deploy="echo -e '\nRunning gulp...\n'; hal && cd public/assets && npm install && ./run_new_gulp && cd ../.. && git add . && git cm -m 'ran gulps' && echo '\n Ran gulp Successfully...\n' && echo '' && echo 'Pushing to Linode(s)...\n' && echo '' && hal_deploy"

alias keep_awake_on="adb shell settings put global stay_on_while_plugged_in 3"
alias keep_awake_off="adb shell settings put global stay_on_while_plugged_in 0"

alias run_migrations_live="echo 'This will Run migrations on SAAS-CUBE!. Make sure the same is pushed there as well'; echo \"\n\"; ssh -t mohamed@linode.adam-cube  '. .bashrc; cd /var/www/halerp.com/public_html/halerp/live; php artisan hal_migrations:run'"
alias run_migrations_dev="ssh -t mohamed@linode.adam-dev  '. .bashrc; cd /var/www/halerp.com/public_html/halerp/dev; php artisan hal_migrations:run --from-code && exit 0 || exit 1'"
alias run_migrations_qa="echo \"\n\"; ssh -t mohamed@linode.adam-issam  '. .bashrc; cd /var/www/halerp.com/public_html/halerp/live; php artisan hal_migrations:run'"


alias random_string="php -r 'echo bin2hex(openssl_random_pseudo_bytes(16));'"
alias kill_emulator="adb -s emulator-5554 emu kill"

# Not alias but usefull things
stty -ixon #Disabel ctrl + s and ctrl + q
#shopt -s autocd # cd to dir by typing only dir name # not needed in zsh 

alias stress_ram='stress -m 1 --vm-bytes 12G --vm-keep'
alias stress_cpu='stress -c 11'

# alias only for mac
if [ `uname` = 'Darwin' ]; then
	alias lsusb='system_profiler SPUSBDataType'
fi

alias yay_upgrade="yay -Syyu --answerupgrade y  --editmenu=false --noremovemake --diffmenu=false --noconfirm"
